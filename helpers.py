import os
from typing import Any
from dataclasses import dataclass

import requests
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib import cm


DATA_URL = 'https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data'
DATA_DIR = 'data'
DATA_PATH = os.path.join(DATA_DIR, 'iris.csv')


def iris_download():
    if os.path.exists(DATA_PATH):
        print('iris data set already exists, no download required')
        return

    print('iris data does not exist, starting download')

    # create directory
    if not os.path.exists(DATA_DIR):
        os.makedirs(DATA_DIR)

    try:
        # stream download
        r = requests.get(DATA_URL, stream=True)
        r.raise_for_status()
        
        with open(DATA_PATH, 'wb') as f:
            for chunk in r.iter_content(chunk_size=4096):
                if chunk:
                    f.write(chunk)
        r.raise_for_status()
    except Exception:
        os.remove(DATA_PATH)
        raise

    print('iris data successfully downloaded')

def plot_model(model, data, show_errors=False, x_col="petal_width", y_col="petal_length"):
    fig, ax = plt.subplots()
    sns.scatterplot(data=data, x=x_col, y=y_col, ax=ax)
    X = data[x_col]
    spacing = [X.min(), X.max()]
    ax.plot(spacing, [model(x) for x in spacing])
    ax.grid(True, which='both')
    
    if show_errors:
        X = data[x_col]
        Y = data[y_col]
        P = [model(x) for x in X]
        for x, y, p in zip(X, Y, P):
            ax.vlines(x, y, p, color="r")
            
def plot_models(history, data, x_col="petal_width", y_col="petal_length"):
    fig, ax = plt.subplots()
    sns.scatterplot(data=data, x=x_col, y=y_col, ax=ax)
    X = data[x_col]
    spacing = [X.min(), X.max()]
    ax.grid(True, which='both')
    
    for model in history:
        ax.plot(spacing, [model(x) for x in spacing])

def plot_w_loss(model_class, loss_func, b, x_min, x_max):
    spacing = np.linspace(x_min, x_max, 100)
    losses = [loss_func(model_class(w, b)) for w in spacing]
    fig, ax = plt.subplots()
    ax.plot(spacing, losses)
    ax.set_xlabel('w')
    ax.set_ylabel('loss')
    return ax

def plot_b_loss(model_class, loss_func, w, x_min, x_max):
    spacing = np.linspace(x_min, x_max, 100)
    losses = [loss_func(model_class(w, b)) for b in spacing]
    fig, ax = plt.subplots()
    ax.plot(spacing, losses)
    ax.set_xlabel('b')
    ax.set_ylabel('loss')
    return ax

def plot_3d_loss(model_class, loss_func, x_min, x_max, opacity=1, xlabel="w", ylabel="b"):
    spacing = np.linspace(x_min, x_max, 100)
    W, B = np.meshgrid(spacing, spacing)
    losses = [loss_func(model_class(w, b)) for w, b in zip(np.ravel(W), np.ravel(B))]
    losses = np.array(losses).reshape(W.shape)
    fig, ax = plt.subplots(figsize=(10,10), subplot_kw={"projection": "3d"})
    ax.plot_surface(W, B, losses, cmap=cm.coolwarm, linewidth=0, alpha=opacity)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_zlabel('loss')
    return ax

def compare_statistics(data, index):
    df = pd.DataFrame(index=index, columns=["min", "max", "mean", "std"])
    for i in df.index:
        df.loc[i]["min"] = np.round(data[i].min(), 2)
        df.loc[i]["max"] = np.round(data[i].max(), 2)
        df.loc[i]["mean"] = np.round(data[i].mean(), 2)
        df.loc[i]["std"] = np.round(data[i].std(), 2)
    return df

def plot_w_pd(model, loss_func, gradient, ax, text_x_offset=0, text_y_offset=0):
    spacing = np.linspace(model.w - 2, model.w + 2, 100)
    error = loss_func(model)
    pd_w, _ = gradient(model)
    Y = [(x - model.w) * pd_w + error for x in spacing]
    ax.plot(spacing, Y)
    ax.plot([model.w], [error], 'bo')
    ax.text(model.w + text_x_offset, error + text_y_offset, f"{pd_w:.2}")

def plot_epochs(history, loss_func):
    losses = [loss_func(model) for model in history]
    fig, ax = plt.subplots()
    ax.plot(range(len(history)), losses)
    ax.set_xlabel('epoch')
    ax.set_ylabel('loss')
    
def plot_3d_training(history, loss_func, ax):
    W = [model.w for model in history]
    B = [model.b for model in history]
    losses = [loss_func(model) for model in history]
    ax.scatter3D(W, B, losses)
    
def plot_3d_training_logistic(history, loss_func, ax):
    W = [model.w1 for model in history]
    B = [model.w2 for model in history]
    losses = [loss_func(model) for model in history]
    ax.scatter3D(W, B, losses)

def plot_sigmoid():
    def sigmoid(t):
        return 1 / (1 + np.exp(-t))
    spacing = np.linspace(-7, 7, 100)
    fig, ax = plt.subplots()
    ax.plot(spacing, [sigmoid(t) for t in spacing]);
    ax.set_xlabel('t')
    ax.set_ylabel('sigmoid(t)')
    
def plot_boundary(data, decision_boundary, x1_col, x2_col):
    fig, ax = plt.subplots()
    sns.scatterplot(data=data, x=x1_col, y=x2_col, hue="species", ax=ax)
    spacing = np.linspace(data[x1_col].min(), data[x1_col].max(), 10)
    boundary_values = np.array([decision_boundary(x1) for x1 in spacing])
    ax.plot(spacing, boundary_values, label='boundary')

@dataclass
class DecisionBoundary:
    model: Any
    threshold: float
    
    def __call__(self, x1):
        return (np.log(self.threshold / (1 - self.threshold)) - x1 * self.model.w1) * (1 / self.model.w2)
    
# source: https://jonchar.net/notebooks/Artificial-Neural-Network-with-Keras/
def plot_decision_contour(X, Y, model, steps=100, cmap='RdBu'):
    cmap = plt.get_cmap(cmap)
    
    xmin, xmax = X[:,0].min() - 1, X[:,0].max() + 1
    ymin, ymax = X[:,1].min() - 1, X[:,1].max() + 1
    x_span = np.linspace(xmin, xmax, steps)
    y_span = np.linspace(ymin, ymax, steps)
    xx, yy = np.meshgrid(x_span, y_span)

    # Make predictions across region of interest
    labels = model.predict(np.c_[xx.ravel(), yy.ravel()])

    # Plot decision boundary in region of interest
    z = labels.reshape(xx.shape)
    z = z > .5
    
    fig, ax = plt.subplots()
    ax.contourf(xx, yy, z, cmap=cmap, alpha=0.5)

    # Get predicted labels on training data and plot
    train_labels = model.predict(X)
    ax.scatter(X[:,0], X[:,1], c=Y, cmap=cmap, lw=0)
    
    return fig, ax
