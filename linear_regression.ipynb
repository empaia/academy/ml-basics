{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "excellent-difference",
   "metadata": {},
   "source": [
    "# EMPAIA Academy: Machine Learning Basics - Part 1: Linear Regression\n",
    "\n",
    "## by Christoph Jansen\n",
    "\n",
    "<br/>\n",
    "\n",
    "## Goals\n",
    "\n",
    "This lecture teaches you some basic principles of\n",
    "\n",
    "* data science,\n",
    "* predictive (machine learning) models,\n",
    "* gradient-based optimization,\n",
    "* and **demystifies Artificial Neural Networks**.\n",
    "\n",
    "This lecture will **NOT** teach you\n",
    "\n",
    "* the difference between training, test, validation data\n",
    "* advanced model evaluation,\n",
    "* overfitting of models,\n",
    "* data bias,\n",
    "* and image processing.\n",
    "\n",
    "## Content\n",
    "\n",
    "* Iris Data:\n",
    "    * Data exploration and plotting\n",
    "* Linear Regression (for continuous predictions)\n",
    "    * Model\n",
    "    * Loss Function (Mean Squared Error)\n",
    "    * Data scaling\n",
    "    * Training\n",
    "* Logistic Regression (for classification)\n",
    "    * Model\n",
    "    * Training\n",
    "* Tensorflow\n",
    "    * Logistic Regression\n",
    "    * Artificial Neural Networks"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "reasonable-telling",
   "metadata": {},
   "source": [
    "## Iris Data\n",
    "\n",
    "Three different species of Iris flowers (Schwertlilie). **Width** and **length** of **petals** (Kronblatt) and **sepals** (Kelchblatt) between species differ.\n",
    "\n",
    "![iris](https://camo.githubusercontent.com/74e378bb24b34efb63e8db09c4f073370d36f23aaa2c7580a805e93c881b78c2/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f6173736574732e6461746163616d702e636f6d2f626c6f675f6173736574732f4d616368696e652b4c6561726e696e672b522f697269732d6d616368696e656c6561726e696e672e706e67)\n",
    "\n",
    "Source: https://github.com/jennifer-ryan/iris-data-set-project"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ordered-administration",
   "metadata": {},
   "source": [
    "This is a **Jupyter Notebook**, a digital lab book for Data Scientists.\n",
    "\n",
    "* contains documentation and code\n",
    "* the code you see is **Python**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "physical-sussex",
   "metadata": {},
   "outputs": [],
   "source": [
    "# import python libraries\n",
    "from dataclasses import dataclass\n",
    "from typing import List\n",
    "\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import seaborn as sns\n",
    "from sklearn.preprocessing import StandardScaler\n",
    "\n",
    "import helpers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "hawaiian-survey",
   "metadata": {},
   "outputs": [],
   "source": [
    "# download iris data set if it does not yet exist\n",
    "helpers.iris_download()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "killing-sleeping",
   "metadata": {},
   "outputs": [],
   "source": [
    "# data preparation\n",
    "data = pd.read_csv(helpers.DATA_PATH, header=None)\n",
    "data.columns = ['sepal_length', 'sepal_width', 'petal_length', 'petal_width', 'species']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "laughing-produce",
   "metadata": {},
   "outputs": [],
   "source": [
    "# take a look at data/iris.csv for the full dataset\n",
    "data.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "yellow-count",
   "metadata": {},
   "outputs": [],
   "source": [
    "data.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "immune-moisture",
   "metadata": {},
   "outputs": [],
   "source": [
    "sns.scatterplot(data=data, x=\"sepal_width\", y=\"sepal_length\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "seasonal-muslim",
   "metadata": {},
   "outputs": [],
   "source": [
    "sns.scatterplot(data=data, x=\"petal_width\", y=\"petal_length\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fundamental-relations",
   "metadata": {},
   "source": [
    "## Linear Regression\n",
    "\n",
    "### Model\n",
    "\n",
    "The linear model is defined as\n",
    "\n",
    "$$\n",
    "model(x) = x \\cdot w + b\n",
    "$$\n",
    "\n",
    "with the model being a function of $x$, where the parameters $w$ and $b$ are treated as constants."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "packed-avatar",
   "metadata": {},
   "outputs": [],
   "source": [
    "@dataclass\n",
    "class LinearModel:\n",
    "    w: int\n",
    "    b: int\n",
    "    \n",
    "    def __call__(self, x):\n",
    "        return x * self.w + self.b"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "minus-piano",
   "metadata": {},
   "outputs": [],
   "source": [
    "bad_model = LinearModel(w=-2, b=6)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "living-faith",
   "metadata": {},
   "outputs": [],
   "source": [
    "bad_model(x=2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "deluxe-banner",
   "metadata": {},
   "outputs": [],
   "source": [
    "bad_model(x=3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "unknown-salmon",
   "metadata": {},
   "outputs": [],
   "source": [
    "bad_model(x=4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "objective-nudist",
   "metadata": {},
   "outputs": [],
   "source": [
    "helpers.plot_model(bad_model, data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "amazing-infrastructure",
   "metadata": {},
   "outputs": [],
   "source": [
    "good_model = LinearModel(w=2.5, b=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "swedish-finland",
   "metadata": {},
   "outputs": [],
   "source": [
    "helpers.plot_model(good_model, data)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "hybrid-special",
   "metadata": {},
   "source": [
    "### Asking (Meaningful?) Questions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "confidential-perry",
   "metadata": {},
   "outputs": [],
   "source": [
    "good_model(x=0.25)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "honest-disco",
   "metadata": {},
   "outputs": [],
   "source": [
    "good_model(x=2.0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "activated-brazil",
   "metadata": {},
   "outputs": [],
   "source": [
    "good_model(x=0.75)  # should I even ask this?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "stainless-meter",
   "metadata": {},
   "outputs": [],
   "source": [
    "good_model(x=-1)  # this doesn't make sense... still getting an answer!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "parental-garbage",
   "metadata": {},
   "source": [
    "### Side Quest\n",
    "\n",
    "Imagine a Machine Learning model trained to distinguish between cats and dogs:\n",
    "\n",
    "![cat_or_dog](images/cat_or_dog.jpg)\n",
    "\n",
    "Source: [Photo by Pixabay on Pexels](https://www.pexels.com/de-de/foto/weisses-und-graues-katzchen-auf-braunem-und-schwarzem-textil-mit-leopardenmuster-45201/) and [Photo by Lucas Andrade on Pexels](https://www.pexels.com/de-de/foto/schwarzweiss-siberian-husky-4681107/)\n",
    "\n",
    "![funky_cat](images/funky_cat.jpg)\n",
    "\n",
    "Source: [Photo by Shashank Kumawat on Pexels](https://www.pexels.com/de-de/foto/nahaufnahme-fotografie-von-brown-hedgehog-auf-grunem-gras-12526/)\n",
    "\n",
    "A Machine Learning model will take the data (numbers) and calculate an output. **Usually** a model is **not built** to answer: **Unknown**.\n",
    "\n",
    "In the **context of the EMPAIA platform** this means: At some point in the process, there must be additional checks to ensure only meaningful data is provided to a Machine Learning model."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "processed-blade",
   "metadata": {},
   "source": [
    "### How bad is a model?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "extensive-endorsement",
   "metadata": {},
   "outputs": [],
   "source": [
    "helpers.plot_model(bad_model, data, show_errors=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "announced-clark",
   "metadata": {},
   "outputs": [],
   "source": [
    "helpers.plot_model(good_model, data, show_errors=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "portable-composition",
   "metadata": {},
   "source": [
    "#### Loss Function: Mean Squared Error\n",
    "\n",
    "The loss function is defined as\n",
    "\n",
    "$$\n",
    "loss(model_{w,b}) = \\frac{1}{2m} \\sum\\limits_{i=1}^{m} (model_{w,b}(x_i) - y_i)^2\n",
    "$$\n",
    "\n",
    "with $x_i \\in X$, $y_i \\in Y$ and $m = |X| = |Y|$. Data $X$, $Y$ is treated as constant."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "portuguese-convention",
   "metadata": {},
   "outputs": [],
   "source": [
    "@dataclass\n",
    "class Loss:\n",
    "    X: List[float]\n",
    "    Y: List[float]\n",
    "    \n",
    "    # Mean Squared Error\n",
    "    def __call__(self, model):\n",
    "        P = [model(x) for x in self.X]\n",
    "        errors = [p - y for p, y in zip(P, self.Y)]\n",
    "        return np.sum(np.square(errors)) / (2 * len(errors))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "alone-condition",
   "metadata": {},
   "outputs": [],
   "source": [
    "loss = Loss(X=data[\"petal_width\"], Y=data[\"petal_length\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "attached-applicant",
   "metadata": {},
   "outputs": [],
   "source": [
    "loss(bad_model)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "swiss-garage",
   "metadata": {},
   "outputs": [],
   "source": [
    "loss(good_model)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "improved-disability",
   "metadata": {},
   "outputs": [],
   "source": [
    "helpers.plot_w_loss(LinearModel, loss, b=1, x_min=-5, x_max=10);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "british-watson",
   "metadata": {},
   "outputs": [],
   "source": [
    "helpers.plot_b_loss(LinearModel, loss, w=2.5, x_min=-5, x_max=10);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bacterial-gamma",
   "metadata": {},
   "outputs": [],
   "source": [
    "helpers.plot_3d_loss(LinearModel, loss, x_min=-5, x_max=10, opacity=0.8);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "recognized-manual",
   "metadata": {},
   "outputs": [],
   "source": [
    "scaler = StandardScaler()\n",
    "data[\"petal_width_scaled\"] = scaler.fit_transform(data[\"petal_width\"].values.reshape(-1, 1)).flatten()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "introductory-secretary",
   "metadata": {},
   "outputs": [],
   "source": [
    "helpers.compare_statistics(data, [\"petal_width\", \"petal_width_scaled\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "unique-preview",
   "metadata": {},
   "outputs": [],
   "source": [
    "loss_scaled = Loss(X=data[\"petal_width_scaled\"], Y=data[\"petal_length\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "legal-bronze",
   "metadata": {},
   "outputs": [],
   "source": [
    "helpers.plot_3d_loss(LinearModel, loss_scaled, x_min=-5, x_max=10, opacity=0.8);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "engaging-helen",
   "metadata": {},
   "source": [
    "## Gradient\n",
    "\n",
    "The partial derivatives (gradient) are used by the Stochastic Gradient Descent optimizer and are defined as\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "\\frac{\\partial}{\\partial w} loss(model_{w, b}) &= \\frac{1}{m}\\sum_{i=1}^{m}(model_{w, b}(x_i) - y_i) \\cdot x_i\\\\\n",
    "\\frac{\\partial}{\\partial b} loss(model_{w, b}) &= \\frac{1}{m}\\sum_{i=1}^{m}(model_{w, b}(x_i) - y_i)\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "with $x_i \\in X, y_i \\in Y$ and $m = |X| = |Y|$. Data $X$, $Y$ is treated as constant."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "interstate-mobility",
   "metadata": {},
   "outputs": [],
   "source": [
    "@dataclass\n",
    "class Gradient:\n",
    "    X: List[float]\n",
    "    Y: List[float]\n",
    "    \n",
    "    def __call__(self, model):\n",
    "        P = [model(x) for x in self.X]\n",
    "        \n",
    "        errors = [(p - y) * x for p, x, y in zip(P, self.X, self.Y)]\n",
    "        pd_w = np.sum(errors) / len(errors)\n",
    "        \n",
    "        errors = [p - y for p, y in zip(P, self.Y)]\n",
    "        pd_b = np.sum(errors) / len(errors)\n",
    "        \n",
    "        return pd_w, pd_b"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "double-contents",
   "metadata": {},
   "outputs": [],
   "source": [
    "gradient = Gradient(X=data[\"petal_width_scaled\"], Y=data[\"petal_length\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "nonprofit-board",
   "metadata": {},
   "outputs": [],
   "source": [
    "m1 = LinearModel(w=-3, b=1)\n",
    "m2 = LinearModel(w=1.7, b=1)\n",
    "m3 = LinearModel(w=6, b=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "completed-singer",
   "metadata": {},
   "outputs": [],
   "source": [
    "# just plotting\n",
    "ax = helpers.plot_w_loss(LinearModel, loss_scaled, b=1, x_min=-5, x_max=10)\n",
    "helpers.plot_w_pd(m1, loss_scaled, gradient, ax, text_x_offset=0.5)\n",
    "helpers.plot_w_pd(m2, loss_scaled, gradient, ax, text_x_offset=-0.5, text_y_offset=2.5)\n",
    "helpers.plot_w_pd(m3, loss_scaled, gradient, ax, text_x_offset=-1.2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "korean-carbon",
   "metadata": {},
   "source": [
    "The gradient tells us the **steepness** of the loss function for different values of $w$ or $b$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "metropolitan-brunei",
   "metadata": {},
   "source": [
    "## Machine Learning: Optimizing the model using Stochastic Gradient Descent\n",
    "\n",
    "Randomly initialize w and b.\n",
    "\n",
    "For a number of epochs repeat:\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "pd\\_w &:= \\frac{\\partial}{\\partial w} loss(model_{w,b})\\\\\n",
    "pd\\_b &:= \\frac{\\partial}{\\partial b} loss(model_{w,b})\\\\\\\\\n",
    "w &:= w - \\alpha * pd\\_w\\\\\n",
    "b &:= b - \\alpha * pd\\_b\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "---\n",
    "\n",
    "Choose a learn rate $\\alpha > 0$ to alter the step size for each training epoch. A small $\\alpha$ results in a slow training, a large $\\alpha$ may break the training process."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "impossible-greece",
   "metadata": {},
   "outputs": [],
   "source": [
    "def sgd(gradient, w, b, alpha, epochs):\n",
    "    history = [LinearModel(w, b)]\n",
    "    \n",
    "    for _ in range(epochs):\n",
    "        pd_w, pd_b = gradient(LinearModel(w, b))\n",
    "        w = w - alpha * pd_w\n",
    "        b = b - alpha * pd_b\n",
    "        \n",
    "        history.append(LinearModel(w, b))\n",
    "        \n",
    "    return history"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "comfortable-boost",
   "metadata": {},
   "outputs": [],
   "source": [
    "history = sgd(gradient, w=-4, b=-4, alpha=0.2, epochs=20)\n",
    "trained_model = history[-1]\n",
    "trained_model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aerial-locator",
   "metadata": {},
   "outputs": [],
   "source": [
    "helpers.plot_epochs(history, loss_scaled)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "abroad-apache",
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = helpers.plot_3d_loss(LinearModel, loss_scaled, x_min=-5, x_max=10, opacity=0.35)\n",
    "helpers.plot_3d_training(history, loss_scaled, ax)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "collective-amber",
   "metadata": {},
   "outputs": [],
   "source": [
    "helpers.plot_models(history, data, x_col=\"petal_width_scaled\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "sustainable-utilization",
   "metadata": {},
   "outputs": [],
   "source": [
    "helpers.plot_model(trained_model, data, show_errors=True, x_col=\"petal_width_scaled\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "sacred-indiana",
   "metadata": {},
   "outputs": [],
   "source": [
    "## choosing alpha too large\n",
    "bad_history = sgd(gradient, w=-4, b=-4, alpha=2.1, epochs=20)\n",
    "bad_history[-1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ongoing-premiere",
   "metadata": {},
   "outputs": [],
   "source": [
    "helpers.plot_epochs(bad_history, loss_scaled)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "overall-scoop",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": ".venv",
   "language": "python",
   "name": ".venv"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
