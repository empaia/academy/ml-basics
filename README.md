# EMPAIA Academy: Machine Learning Basics

Author: Christoph Jansen, Charité - Universitätsmedizin Berlin

This repository is a supplement for the [EMPAIA Academy: Machine Learning Basics](https://av.tib.eu/media/51669) Video lecture (Language: German; [DOI:10.5446/51669](https://doi.org/10.5446/51669)).

## Setup

* The following instructions have been tested to work with Python 3.8
  * As of March 2021, not all of the required Python packages work with Python 3.9
* Download and extract (or `git clone`) this software repository into a directory `ml-basics`

### Instruction on Ubuntu 20.04

Run the following commands in `bash`:

```bash
# install python3 and the venv package
sudo apt install python3-venv
# go to repostory directory (adapt directory path if necessary)
cd ml-basics
# create virtual environment
python3 -m venv .venv
# activate virtual environment
source .venv/bin/activate
# install third-party Python libraries
pip install -r requirements.txt
# create a kernel for jupyter-lab in the venv
ipython kernel install --user --name=.venv
# run jupyter-lab, browser should open automatically
jupyter-lab
```

If the setup has been completed once, only a few steps are necessary to open jupyter-lab later again.

```bash
cd ml-basics
source .venv/bin/activate
jupyter-lab
```

### Instructions on Windows 10

Install [Python 3.8 64 bit](https://www.python.org/ftp/python/3.8.8/python-3.8.8-amd64.exe). During the installation process select "Add Python to PATH" and restart the PC afterwards.

By default Windows Powershell disables the execution of scripts. Therefore it is required to open a **Powershell as Administrator** (in the start menu right click on Powershell) and run the following command:

```powershell
set-executionpolicy remotesigned
# type A and press Enter to allow all script executions
```

The Powershell with Administrator privileges can be closed afterwards.

Open a new Powershell without Administrator privileges and run the following commands:

```powershell
# go to repostory directory (adapt directory path if necessary)
cd ml-basics
# create virtual environment
python -m venv .venv
# activate virtual environment
.\.venv\Scripts\activate.ps1
# install third-party Python libraries
pip install -r requirements.txt
# create a kernel for jupyter-lab in the venv
ipython kernel install --user --name=.venv
# run jupyter-lab, browser should open automatically
jupyter-lab
```

If the setup has been completed once, only a few steps are necessary to open jupyter-lab later again.

```powershell
cd ml-basics
.\.venv\Scripts\activate.ps1
jupyter-lab
```

## Jupyter-Notebooks

The Jupyter Notebooks should be viewed in the following order:

1. linear_regression.ipynb
2. logistic_regression.ipynb
3. tensorflow.ipynb
